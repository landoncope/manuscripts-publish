# Manuscripts Publish

Publish a package to a NPM registry if the current version hasn't already been published.

## Usage

`npx @manuscripts/publish`
