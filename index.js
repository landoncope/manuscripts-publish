#!/usr/bin/env node

const { execSync } = require('child_process')
const fetch = require('node-fetch')
const { resolve } = require('path')
const { writeFileSync } = require('fs')
const { name, version } = require(resolve('./package.json'))

if (!name || !version) {
  throw new Error('Could not read name or version from package.json')
}

if (!process.env.NPM_TOKEN) {
  throw new Error('Environment variable NPM_TOKEN is not defined')
}

const publish = function () {
  console.log(`Publishing version ${version} of ${name}`)

  writeFileSync('.npmrc', `//registry.npmjs.org/:_authToken=${process.env.NPM_TOKEN}`);

  execSync('npm publish --access public')
}

fetch(`https://registry.npmjs.org/${name}`).then(function (response) {
  switch (response.status) {
    case 404:
      return publish()

    case 200:
      return response.json().then(function (data) {
        if (!data || !data.versions) {
          throw new Error('No versions found')
        }

        if (version in data.versions) {
          console.log(`Already published version ${version}`)
        } else {
          return publish()
        }
      })

    default:
      throw new Error(response.statusText)
  }
}).catch(error => {
  console.error(error)
  process.exitCode = 1
})
